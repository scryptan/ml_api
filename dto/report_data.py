from dataclasses import dataclass


@dataclass
class ReportData:
    title: str
    region_name: str
    link: str
    criteries: list[str]
