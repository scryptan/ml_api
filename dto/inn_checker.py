from dto.customer import Customer


def check_inn(user: Customer) -> bool:
    inn_length = len(str(user.inn))
    if inn_length == 10:
        return __check_ten_inn(user.inn)
    elif inn_length == 12:
        return __check_twelve_inn(user.inn)

    raise ValueError("ИНН должен быть 10 или 12 символов")


def __check_ten_inn(inn: int) -> bool:
    coefficients = [2, 4, 10, 3, 5, 9, 4, 6, 8]
    compare_val = __calc_control_num(inn, coefficients) % 11

    return (compare_val % 10) == (inn % 10)


def __check_twelve_inn(inn: int) -> bool:
    first_coefficients = [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]
    first_control_val = __calc_control_num(inn, first_coefficients) % 11
    second_coefficients = [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]
    second_control_val = __calc_control_num(inn, second_coefficients) % 11

    return ((first_control_val % 100) // 100) == ((inn % 100) // 100) and (second_control_val % 10) == (inn % 10)


def __calc_control_num(inn: int, coefficients: [int]):
    inn_arr = [*map(int, str(inn))]
    control_num = 0
    for i in range(len(coefficients)):
        control_num = control_num + (coefficients[i] * inn_arr[i])

    return control_num
