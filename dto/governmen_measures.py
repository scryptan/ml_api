from dataclasses import dataclass


@dataclass
class GovernmentMeasures:
    name: str
    document_name: str
