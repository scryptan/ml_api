from __future__ import annotations

from dataclasses import dataclass
from datetime import date


@dataclass
class Report:
    filename: str
    create_date: date
