from dataclasses import dataclass


@dataclass
class Customer:
    inn: int
    email: str
    region: str
