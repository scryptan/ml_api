FROM scryptan/ml_api_base

WORKDIR /app
COPY . /app

RUN poetry install --without dev

ENTRYPOINT ["sh", "start.sh"]