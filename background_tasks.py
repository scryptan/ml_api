from dto.customer import Customer
from dto.governmen_measures import GovernmentMeasures
from dto.report_data import ReportData
from storages.db_connection import get_criteria_from_db, get_subventions_by_ids_from_db, \
    get_document_by_id_from_db, create_user_result_record_in_db
from templates.report_generator import generate_pdf
import json
import asyncio

start_fields_to_soft_search = ['v']
start_fields_to_hard_search = ['org']


async def match_customer_to_data(customer: Customer, data_to_search: dict, progress: set[int],
                                 filepath: str):
    if customer.inn in progress:
        print(f'Customer with inn: {customer.inn} already in progress')
        return

    progress.add(customer.inn)

    criteries = get_criteria_from_db()
    subvention_ids_for_report: list[int] = []
    hard_subvention_ids_for_report: list[int] = []

    for field in start_fields_to_hard_search:
        depth = find_all_depth_values(field, data_to_search)[-1]
        subvention_ids = map(lambda x: x.subvention_id, list(filter(lambda e: depth in e.entity_type, criteries)))
        hard_subvention_ids_for_report.extend(subvention_ids)

    for field in start_fields_to_soft_search:
        depth_values = find_all_depth_values(field, data_to_search)
        depth_values.reverse()
        for depth in depth_values:
            subvention_ids = map(lambda x: x.subvention_id, list(filter(lambda e: depth in e.entity_type, criteries)))
            soft_found_ids = [sub_id for sub_id in subvention_ids if sub_id in hard_subvention_ids_for_report]
            if len(soft_found_ids) > 0:
                subvention_ids_for_report.extend(soft_found_ids)
                break

    if len(subvention_ids_for_report) == 0:
        subvention_ids_for_report.extend(hard_subvention_ids_for_report)

    subventions = list(filter(lambda sub: sub.region_name == customer.region, get_subventions_by_ids_from_db(tuple(set(subvention_ids_for_report)))))
    subventions_to_render: list[ReportData] = []

    for subvention in subventions:
        print(subvention)
        doc = get_document_by_id_from_db(subvention.document_id)
        report_data = ReportData(subvention.title,
                                 subvention.region_name, doc.link, list(
                map(lambda x: x.entity_value,
                    filter(lambda e: e.subvention_id == subvention.id, criteries))))
        subventions_to_render.append(report_data)

    generate_pdf(subventions_to_render, GovernmentMeasures("asd", "dsa"), filepath)

    for subvention in subventions:
        create_user_result_record_in_db(customer.inn, subvention.id, json.dumps(data_to_search))

    progress.remove(customer.inn)


def find_all_depth_values(field: str, data_to_search: dict) -> list[str]:
    fields = [field]
    while field in data_to_search.keys() and field != data_to_search[field]:
        field = data_to_search[field]
        fields.append(field)

    return fields


if __name__ == '__main__':
    data_to_srch = {
        "email": "email@eaw.asd",
        "inn": "174221444658",
        "region": "Республика Марий Эл",
        "reg": "reg-msp",
        "org": "org-ip",
        "v": "v-selh",
        "v-selh": "v-selh-rast",
        "v-tur": "v-tur-blagoustr",
        "secret": "1b3a9374-1a8e-434e-90ab-21aa7b9b80e7"
    }
    asyncio.run(match_customer_to_data(Customer(123213, "email", data_to_srch['region']), data_to_srch, set(), './'))
    print(find_all_depth_values('region', data_to_srch)[-1])
