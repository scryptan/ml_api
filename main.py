# command to start with gunicorn:
# gunicorn main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:80 --certfile=/etc/letsencrypt/live/88.210.6.3.nip.io/fullchain.pem --keyfile=/etc/letsencrypt/live/88.210.6.3.nip.io/privkey.pem --bind 0.0.0.0:443
from __future__ import annotations
from fastapi import FastAPI, HTTPException, BackgroundTasks, Body
from starlette.responses import RedirectResponse
from starlette.staticfiles import StaticFiles
from background_tasks import match_customer_to_data
from dto.inn_checker import Customer, check_inn
from dto.report import Report
from storages.customer_storage import CustomerStorage
from templates.report_filename_generator import generate_filename_by_inn
from datetime import date
from storages.db_connection import get_regions_from_documents_db, get_user_results_from_db, \
    get_subventions_by_ids_from_db, get_documents_by_ids_from_db

app = FastAPI()
customer_storage = CustomerStorage()
inns_in_progress = set()
inn_in_report = dict[int, list[Report]]()
report_folder_path = 'reports'


@app.get("/")
async def index():
    return RedirectResponse(url="/index.html")


@app.get("/regions")
async def index():
    return get_regions_from_documents_db()


@app.get("/user-results")
async def index(key: str = None):
    if key != "very_secret_key":
        raise HTTPException(status_code=405)

    user_results = get_user_results_from_db()
    subvention_ids = set([x.subvention_id for x in user_results])
    subventions = get_subventions_by_ids_from_db(tuple(subvention_ids))
    doc_ids = [x.document_id for x in subventions]
    documents = get_documents_by_ids_from_db(tuple(set(doc_ids)))
    unique_inns = set([x.inn for x in user_results])
    res = []

    for inn in unique_inns:
        grouped = [x for x in user_results if x.inn == inn]
        res_item = {'inn': inn, 'user_input': grouped[0].user_input, 'subventions': []}

        for user_result in grouped:
            subvention = next((e for e in subventions if e.id == user_result.subvention_id))
            document = next((e for e in documents if e.id == subvention.document_id))
            subvention.document_link = document.link
            res_item['subventions'].append(subvention)

        res.append(res_item)

    return res


@app.post("/api/user")
async def setup_user(background_task: BackgroundTasks, payload: dict = Body(...)):
    customer = Customer(int(payload['inn']), payload['email'], payload['region'])

    try:
        check_inn(customer)
    except ValueError:
        raise HTTPException(status_code=400, detail="Incorrect inn")

    search = customer_storage.search(customer.inn)
    if not search:
        customer_storage.create(customer)

    reports = inn_in_report.get(customer.inn)

    if not reports:
        report = create_today_report(customer, payload, background_task)
        inn_in_report.update({customer.inn: [report]})
    else:
        report = get_today_report(reports)
        if not report:
            report = create_today_report(customer, payload, background_task)
            reports.append(report)

    return {"filepath": get_filepath(report)}


app.mount("/", StaticFiles(directory="static"), name="static")


def get_filepath(report: Report) -> str:
    return f'/{report_folder_path}/{report.filename}'


def get_today_report(reports: list[Report]) -> Report | None:
    for report in reports:
        if report.create_date == date.today():
            return report

    return None


def create_today_report(customer: Customer, payload: dict, background_task: BackgroundTasks) -> Report:
    report = Report(generate_filename_by_inn(customer), date.today())
    background_task.add_task(match_customer_to_data, customer, payload, inns_in_progress,
                             get_filepath(report))
    return report
