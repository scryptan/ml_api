import abc


class StorageBase(abc.ABC):
    @abc.abstractmethod
    def create(self, model):
        raise NotImplemented

    @abc.abstractmethod
    def search(self, model_id):
        raise NotImplemented

    @abc.abstractmethod
    def update(self, new_model):
        raise NotImplemented

    @abc.abstractmethod
    def delete(self, model_id) -> None:
        raise NotImplemented
