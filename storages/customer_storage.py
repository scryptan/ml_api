from __future__ import annotations

from dto.customer import Customer
from storages.storage_base import StorageBase


class CustomerStorage(StorageBase):
    def __init__(self):
        self.customers = []
        self.customer_ids = set()

    def create(self, customer: Customer) -> Customer:
        if customer.inn not in self.customer_ids:
            self.customers.append(customer)
            self.customer_ids.add(customer.inn)
        else:
            raise ValueError("INN should be unique")

        return customer

    def search(self, model_id: int) -> Customer | None:
        if model_id not in self.customer_ids:
            return None

        for cust in self.customers:
            if cust.inn == model_id:
                return cust

        raise BufferError("Inn in id list but customer with this inn doesn't exist")

    def update(self, customer: Customer) -> Customer:
        old = self.search(customer.inn)
        if not old:
            raise ValueError("Can't update customer that doesn't exist")

        self.customers.remove(old)
        self.customers.append(customer)
        return customer

    def delete(self, model_id: int):
        old = self.search(model_id)
        if not old:
            raise ValueError("Can't update customer that doesn't exist")

        self.customers.remove(old)
