from __future__ import annotations

from dataclasses import dataclass
from datetime import date

import psycopg2
from typing import Tuple
from psycopg2 import Error


@dataclass
class UserResult:
    inn: int
    subvention_id: int
    user_input: dict


@dataclass
class Criteria:
    id: int
    subvention_id: int
    entity_type: str
    entity_value: str


@dataclass
class Subvention:
    id: int
    document_id: int
    sum: int
    title: str
    vr: str
    csr: str
    grbs: str
    region_name: str
    document_link: str | None


@dataclass
class Document:
    id: int
    link: str
    title: str
    order_number: int
    order_date: date
    last_edit_date: str
    region_name: str


@dataclass
class DocumentText:
    document_id: int
    chunk_id: int
    chunk: str


def get_db_connection():
    connection = psycopg2.connect()
    return connection


def get_document_texts_from_db() -> list[DocumentText]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM document_text"""
        cursor.execute(postgres_insert_query)
        return list(map(lambda x: DocumentText(x[0], x[1], x[2]), cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_documents_from_db() -> list[Document]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM documents"""
        cursor.execute(postgres_insert_query)
        return list(map(lambda x: Document(x[0], x[1],
                                           x[2], x[3],
                                           x[4], x[5],
                                           x[6]), cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_regions_from_documents_db() -> list[str]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT DISTINCT region_name FROM documents"""
        cursor.execute(postgres_insert_query)
        return list(map(lambda x: x[0], cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_documents_by_ids_from_db(ids: Tuple[int]) -> list[Document]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM documents where id in %s"""
        cursor.execute(postgres_insert_query, (ids,))
        return list(map(lambda x: Document(x[0], x[1],
                                           x[2], x[3],
                                           x[4], x[5],
                                           x[6]), cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_document_by_id_from_db(doc_id: int) -> Document:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM documents where id = %s"""
        cursor.execute(postgres_insert_query, (str(doc_id),))
        doc_fetched = cursor.fetchone()
        return Document(doc_fetched[0], doc_fetched[1],
                        doc_fetched[2], doc_fetched[3],
                        doc_fetched[4], doc_fetched[5],
                        doc_fetched[6])

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_subventions_from_db() -> list[Subvention]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM subvention"""
        cursor.execute(postgres_insert_query)
        return list(map(lambda x: Subvention(x[0], x[1],
                                             x[2], x[3],
                                             x[4], x[5],
                                             x[6], x[7], None), cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_subventions_by_ids_from_db(ids: Tuple[int]) -> list[Subvention]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM subvention where id in %s"""
        cursor.execute(postgres_insert_query, (ids,))
        return list(map(lambda x: Subvention(x[0], x[1],
                                             x[2], x[3],
                                             x[4], x[5],
                                             x[6], x[7], None), cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_criteria_from_db() -> list[Criteria]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM criteria"""
        cursor.execute(postgres_insert_query)
        return list(map(lambda x: Criteria(x[0], x[1], x[2], x[3]), cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def create_user_result_record_in_db(inn: int, subvention_id: int, user_data: str) -> None:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """INSERT INTO user_result 
        (inn, subvention_id, user_input) 
        VALUES (%s,%s,%s) returning inn"""
        record_to_insert = (inn, subvention_id, user_data)
        cursor.execute(postgres_insert_query, record_to_insert)
        connection.commit()

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()


def get_user_results_from_db() -> list[UserResult]:
    connection = get_db_connection()
    cursor = connection.cursor()
    try:
        postgres_insert_query = """SELECT * FROM user_result"""
        cursor.execute(postgres_insert_query)
        return list(map(lambda x: UserResult(x[0], x[1], x[2]), cursor.fetchall()))

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
