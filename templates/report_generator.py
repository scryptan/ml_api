import jinja2
import pdfkit

from dto.report_data import ReportData
from dto.governmen_measures import GovernmentMeasures

static_files_folder_path = './static'


def generate_pdf(report_data: list[ReportData], user: GovernmentMeasures, filepath: str) -> None:

    context = {'subventions': report_data, 'user': user.name}

    template_loader = jinja2.FileSystemLoader('templates')
    template_env = jinja2.Environment(loader=template_loader)

    template = template_env.get_template('template.html')
    output_text = template.render(context)

    config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf')
    pdfkit.from_string(output_text, get_filepath(filepath), configuration=config)


def get_filepath(filename: str):
    return f'{static_files_folder_path}/{filename}'
