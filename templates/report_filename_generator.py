from datetime import datetime
from dto.customer import Customer


def generate_filename_by_inn(customer: Customer) -> str:
    return f'{customer.inn}_{datetime.now().strftime("%d-%m-%Y_%H:%M:%S")}.pdf'
